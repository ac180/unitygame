public var health : int;
public var h1 : Texture;
public var h2 : Texture;
private var i:int;
private var healthW:int;
public var dead;
public var deathSound : AudioClip;
public var deathEffect : GameObject;
function Start()
{
	//print("player health start");
	health = 10;
	healthW = Screen.width/20;
	dead=0;
}

function Update () 
{
	
	if(health<=0)
	{
		this.gameObject.transform.position.y=-10;
		audio.PlayOneShot(deathSound);
		//yield new WaitForSeconds (5);
		
		health = 10;
		this.gameObject.transform.position=Vector3(11,1.1,11);
	}
}
/*function OnControllerColliderHit (hit : ControllerColliderHit) {
    var body : Rigidbody = hit.collider.attachedRigidbody;
    print("being hit");
    // no rigidbody
    if (body == null || body.isKinematic)
        return;
        
    // We dont want to push objects below us
    if (body.name=="enemybullet")
    {
    	print("hit by body.name");
        health--;
    }   
    //near attack 
    
    
}*/
function beingHit()
{
	health--;
	if(health<=0)
	{
		health = 10;
		dead=1;
		audio.PlayOneShot(deathSound);
		Instantiate(deathEffect, gameObject.transform.position, Quaternion.identity);
		this.gameObject.transform.position.y=-10;
		yield new WaitForSeconds (5);
		this.gameObject.transform.position=Vector3(11,1.1,11);
		dead=0;
	}
}
function OnGUI() 
{
    if(!h1||!h2){
        Debug.LogError("Assign a Texture in the inspector.");
        return;
    }	
    for(i=0;i<health;i++)
    	GUI.DrawTexture(Rect(i*healthW,0,healthW,healthW), h1, ScaleMode.ScaleToFit, true, 0);
    for(i=health;i<10;i++)
    	GUI.DrawTexture(Rect(i*healthW,0,healthW,healthW), h2, ScaleMode.ScaleToFit, true, 0);
}