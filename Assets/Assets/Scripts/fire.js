var BulletPrefab : Rigidbody;//子彈
var _ballSpeed = 10;         //子彈速度
var mouseHoldDown : boolean = false;
var frequency : float = 5;  //子彈頻率

private var lastFireTime : float = -1;
private var gun : GameObject;
private var bullet : Rigidbody ; 




function Update(){
	var idan:Vector3[] = new Vector3[5];
	var i:int;
	var L:int;
	if(Input.multiTouchEnabled){
			L=Input.touchCount;
			for(i=0;i<L;i++) idan[i] = Input.touches[i].position;
		}else{
			if (Input.GetMouseButtonDown(0)){
				mouseHoldDown = true;}
			else if(Input.GetMouseButtonUp(0)){
				mouseHoldDown = false;
			}
			L=0;
			if(mouseHoldDown){
				L=1;
				idan[0] = Input.mousePosition;
				//print(L + " " + idan[0]);
			}
	}
 	for(i=0;i<L;i++){
	 	var ScreenW = Screen.width;
	 	
	 	if(idan[i].x>ScreenW/2){
	 		if (idan[i].x > 127 & idan[i].z < 480){
		 	    if (Time.time > lastFireTime + 2 / frequency) {//子彈發射速率
		 		  	fireBullet();//子彈發射函式 
		 		  	lastFireTime = Time.time;
				}
			}       				
		  		 		
	 	}
 	
 	}

}
function fireBullet(){
    bullet  = Instantiate (BulletPrefab, transform.position , Quaternion.identity); 
	bullet.name = "bullet";
	bullet.transform.rotation = Quaternion.LookRotation(transform.TransformDirection(Vector3.right));
	bullet.velocity = transform.TransformDirection(Vector3.back* _ballSpeed);//子彈發射方向的速度 
	Physics.IgnoreCollision(transform.collider, bullet.transform.collider);
}

