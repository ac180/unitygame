var PauseMenu : Texture2D;
var ResumeTexture : Texture2D;
var ResumeMOTexture : Texture2D;
var RestartTexture : Texture2D;
var RestartMOTexture : Texture2D;
var QuitTexture : Texture2D;
var QuitMOTexture : Texture2D;
public static var GameStopped : boolean = false;

function Update () {
	if ( Input.GetKey(KeyCode.Menu) || Input.GetKeyDown(KeyCode.P) )
	{
  		 if(GameStopped == false){
     		Time.timeScale = 0;
     		GameStopped = true;
  		}
	}
}

function OnGUI(){
	if(GameStopped == true){
		var GUITmp : GUIStyle = GUIStyle();
		
		GUI.DrawTexture(Rect(Screen.width*0.25 ,Screen.height*0.25 ,PauseMenu.width*0.45*(Screen.width/529.0) ,PauseMenu.height*0.8*(Screen.height/719.0) ), PauseMenu, ScaleMode.StretchToFill, true, 0);
	
		GUITmp.normal.background = ResumeTexture;
		GUITmp.active.background = ResumeMOTexture;
		if (GUI.Button(Rect(Screen.width* (450.0/1444.0) ,Screen.height* (350.0/963.0) ,  ResumeTexture.width*(407.0/418.0)*(Screen.width/1440.0)  , ResumeTexture.height*(132.0/148.0)*(Screen.height/963.0) ),"",GUITmp)){
			Time.timeScale = 1;
     		GameStopped = false;
		}
		
		GUITmp.normal.background = RestartTexture;
		GUITmp.active.background = RestartMOTexture;
		if (GUI.Button(Rect(Screen.width* (450.0/1444.0) ,Screen.height* (500.0/963.0) ,  RestartTexture.width*(407.0/418.0)*(Screen.width/1440.0)  , RestartTexture.height*(132.0/148.0)*(Screen.height/963.0) ),"",GUITmp)){
			Time.timeScale = 1;
     		GameStopped = false;
			Application.LoadLevel("Game");	//改這進入遊戲 將Menu改成要進入的scene
		}
		
		GUITmp.normal.background = QuitTexture;
		GUITmp.active.background = QuitMOTexture;
		if (GUI.Button(Rect(Screen.width* (450.0/1444.0) ,Screen.height* (650.0/963.0) ,  QuitTexture.width*(407.0/418.0)*(Screen.width/1440.0)  , QuitTexture.height*(132.0/148.0)*(Screen.height/963.0) ),"",GUITmp)){
			Time.timeScale = 1;
     		GameStopped = false;
			Application.LoadLevel("Menu");
		}
		
	}
	
	
}