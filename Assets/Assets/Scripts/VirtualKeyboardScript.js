var LeftPadBall : Texture2D;
var LeftPadBG : Texture2D;
var mouseHoldDown : boolean = false;
public var factor : float = 0.1;
public  var objPlayer : GameObject;
public var objCamera : GameObject;
public  var  direction : Vector3;
//public  var  moveSpeed :float= 10f;
//public var tempVector : Vector3;
public var movement : Vector3;
private var Ay; 
public var play:GameObject;
public var speeddown:float=5.0;
public var state:int;

function Update () {
	
//	ProcessMovement();
	HandleCamera();

	
}

function Start() {
	play=this.gameObject;
	state=0;
//	objPlayer = GameObject.FindWithTag ("Player");
	objCamera = GameObject.FindWithTag ("MainCamera");
	Ay=this.transform.position.y;
//	print(LeftPadBG.width/2);
//	print(LeftPadBG.height/2);
//	print(Screen.width);
//	print(Screen.height);
}

function OnGUI(){
	var sp:playerhealth = play.GetComponent("playerhealth");
	var ScreenW = Screen.width;
	var ScreenH = Screen.height;

  	GUI.DrawTexture(Rect(ScreenW * 0.01 , ScreenH * 0.7 ,LeftPadBG.width/1.5  ,LeftPadBG.height/1.5), 
	  					LeftPadBG, ScaleMode.StretchToFill, true, 0);
	var mousePos = Input.mousePosition;
	var tmpmousePose = mousePos;
 	var idan:Vector3[] = new Vector3[5];
	var i:int;
	var j:int;
	var L:int;
	if(Time.timeScale)
	{
		if(Input.multiTouchEnabled){
			L=Input.touchCount;
			for(i=0;i<L;i++) idan[i] = Input.touches[i].position;
		}else{
			if (Input.GetMouseButtonDown(0)){
				mouseHoldDown = true;}
			else if(Input.GetMouseButtonUp(0)){
				mouseHoldDown = false;
			}
			L=0;
			if(mouseHoldDown){
				L=1;
				idan[0] = Input.mousePosition;
				//print(L + " " + idan[0]);
				}
		}
	}
	if(L==0&L!=2)
	{
		GUI.DrawTexture(Rect(ScreenW * 0.01+LeftPadBG.width/3-LeftPadBall.width/2,ScreenH * 0.7 + LeftPadBG.height/3-LeftPadBall.height/2,LeftPadBall.width ,LeftPadBall.height ), 
	  					LeftPadBall, ScaleMode.StretchToFill, true, 0);
	  	state=0;
	}
	var test:boolean=false;
	var turnaround:boolean=false;
 	for(i=0;i<L;i++){
 		for(j=0;j<L;j++){
	 		if(idan[j].x>ScreenW/2)
	 		{
	 			turnaround = true;
	 		}
	 		}
	 	if(idan[i].x<ScreenW/2 & !sp.dead){
	 		idan[i].y = ScreenH - idan[i].y;
	
			
			movement.x = idan[i].x - (ScreenW * 0.01+ LeftPadBG.width/3);
			movement.z = (ScreenH * 0.7 + LeftPadBG.height/3)-idan[i].y;
			movement = movement /(LeftPadBG.width/2);
			if(movement.x>1)
				movement.x=1;
			else if(movement.x<-1)
				movement.x=-1;
			if(movement.z>1)
				movement.z=1;
			else if(movement.z<-1)
				movement.z=-1;
			movement/=speeddown;
			ProcessMovement();
			if(!turnaround)
			{
				transform.rotation = Quaternion.LookRotation(movement);
				transform.eulerAngles =  Vector3(0,transform.eulerAngles.y + 180,0);
			}
		test = true; 
	  	GUI.DrawTexture(Rect(idan[i].x - LeftPadBall.width/2 ,idan[i].y - LeftPadBall.height/2,LeftPadBall.width  ,LeftPadBall.height), 
	  					LeftPadBall, ScaleMode.StretchToFill, true, 0);
	 					
 		}else if(!test&L<2){
 			
	  		GUI.DrawTexture(Rect(ScreenW * 0.01+LeftPadBG.width/3-LeftPadBall.width/2,ScreenH * 0.7 + LeftPadBG.height/3-LeftPadBall.height/2,LeftPadBall.width ,LeftPadBall.height ), 
	  					LeftPadBall, ScaleMode.StretchToFill, true, 0);
 		}

 	}


}
function ProcessMovement()
{
//	direction = Vector3.zero;
//	direction.x = Input.GetAxis("Horizontal");
//	direction.z = Input.GetAxis("Vertical");
	if(state!=2)
		state=1;
    var controller : CharacterController = GetComponent(CharacterController);
	var collisionFlags = controller.Move(movement);
	if(collisionFlags==1){transform.position -= (movement * factor);}
	else{transform.position += (movement * factor);}
	if(transform.position.y!=Ay)
	{
		transform.position.y = Ay;
		transform.position -= movement*factor;
	}
	
	
//	tempVector = rigidbody.GetPointVelocity(transform.position) * Time.deltaTime * 1000;
//	rigidbody.AddForce (-tempVector.x, -tempVector.y, -tempVector.z);
//	rigidbody.AddForce (inputMovement.normalized * moveSpeed * Time.deltaTime);
//	transform.rotation = Quaternion.LookRotation(inputRotation);
//	transform.eulerAngles =  Vector3(0,transform.eulerAngles.y + 180,0);
//	transform.position +=  Vector3(movement.x,0,movement.z);
}
function HandleCamera()
{
	objCamera.transform.position =  Vector3(transform.position.x,4,transform.position.z-3);
	objCamera.transform.eulerAngles =  Vector3(40,0,0);
}
