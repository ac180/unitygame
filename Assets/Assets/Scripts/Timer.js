@script ExecuteInEditMode

var total : int = 80;
private var start : int;
private var remain : int;
private var min : int;
private var sec : int;

var mystyle : GUIStyle;

function OnGUI () {
	
	start = Time.timeSinceLevelLoad;
	remain = total-start;
	min = remain / 60;
	sec = remain % 60;	
	
	if( sec <10)
		GUI.Label(Rect(Screen.width-150,30,100,100), "Time: " + min.ToString() + ":0" + sec.ToString() , mystyle);
	else	
		GUI.Label(Rect(Screen.width-150,30,100,100), "Time: " + min.ToString() + ":" + sec.ToString() ,mystyle);
	
	
	if ( remain == 0 )
		Application.LoadLevel("GameOver");	//改這進入遊戲 將Menu改成要進入的scene
}