var BGTexture : Texture2D;
var BackTexture : Texture2D;
var BackMOTexture : Texture2D;
var RightTexture : Texture2D;
var RightMOTexture : Texture2D;
var LeftTexture : Texture2D;
var LeftMOTexture : Texture2D;
var People1Texture : Texture2D;
var People1Power : Texture2D;
var People2Texture : Texture2D;
var People2Power : Texture2D;
var People3Texture : Texture2D;
var People3Power : Texture2D;
var circleTexture : Texture2D;
public static var actor : int;	//用來標示角色 0:男生 1:女生 2:另一個男生

function Start(){
	 
	actor = 0;
}

function OnGUI(){
	
	var GUITmp : GUIStyle = GUIStyle();
	
	GUI.DrawTexture(Rect(0 ,0 ,Screen.width ,Screen.height), BGTexture, ScaleMode.StretchToFill, true, 0);
	
	//回到menu
	GUITmp.normal.background = BackTexture;
	GUITmp.active.background = BackMOTexture;
	if (GUI.Button(Rect(Screen.width * (1229.0/1444.0) , Screen.height * (797.0/963.0) ,  BackTexture.width*(Screen.width/1440.0)  , BackTexture.height*(Screen.height/963.0) ),"",GUITmp)){
		Application.LoadLevel("Menu");
	}	
	//向右選
	GUITmp.normal.background = RightTexture;
	GUITmp.active.background = RightMOTexture;
	if (GUI.Button(Rect(Screen.width * (1269.0/1444.0) , Screen.height * (426.0/963.0) ,  RightTexture.width*(Screen.width/1440.0)  , RightTexture.height*(Screen.height/963.0) ),"",GUITmp)){
		actor = (actor+1) %3;
	}
	//向左選
	GUITmp.normal.background = LeftTexture;
	GUITmp.active.background = LeftMOTexture;
	if (GUI.Button(Rect(Screen.width * (654.0/1444.0) , Screen.height * (426.0/963.0) ,  LeftTexture.width*(Screen.width/1440.0)  , LeftTexture.height*(Screen.height/963.0) ),"",GUITmp)){
		actor = (actor+2) %3;
	}

	//人腳下的圓
	GUI.DrawTexture(Rect(Screen.width * (765.0/1444.0) , Screen.height * (700.0/963.0) ,  circleTexture.width*(520.0/598.0)*(Screen.width/1440.0)  , circleTexture.height*(220.0/311.0)*(Screen.height/963.0) ), circleTexture, ScaleMode.ScaleToFit, true, 0);

	if( actor == 0){
		//GUI.DrawTexture(Rect(Screen.width * 0.5 , Screen.height * 0.22 ,  People2Texture.width*0.2  , People2Texture.height*0.2 ), People2Texture, ScaleMode.ScaleToFit, true, 0);
		//GUI.DrawTexture(Rect(Screen.width * 0.72 , Screen.height * 0.22 ,  PeopleTexture.width*0.125  , PeopleTexture.height*0.125 ), PeopleTexture, ScaleMode.ScaleToFit, true, 0);

		GUITmp.normal.background = People1Texture;
		GUITmp.active.background = People1Texture;
		if (GUI.Button(Rect(Screen.width * (914.0/1444.0) , Screen.height * (200.0/963.0) ,  People1Texture.width*(256.0/177.0)*(Screen.width/1440.0)  , People1Texture.height*(450.0/395.0)*(Screen.height/963.0) ),"",GUITmp)){
			Application.LoadLevel("Game");	//改這進入遊戲 將Menu改成要進入的scene
		}
		GUI.DrawTexture(Rect(Screen.width * (10.0/1444.0) , Screen.height * (400.0/963.0) ,  People1Power.width*(742.0/843.0)*(Screen.width/1440.0)  , People1Power.height*(800.0/662.0)*(Screen.height/963.0) ), People1Power, ScaleMode.ScaleToFit, true, 0);
	}
	
	if( actor == 1){
		//GUI.DrawTexture(Rect(Screen.width * 0.5 , Screen.height * 0.22 ,  People1Texture.width*0.2  , People1Texture.height*0.2 ), People1Texture, ScaleMode.ScaleToFit, true, 0);
		//GUI.DrawTexture(Rect(Screen.width * 0.72 , Screen.height * 0.22 ,  PeopleTexture.width*0.125  , PeopleTexture.height*0.125 ), PeopleTexture, ScaleMode.ScaleToFit, true, 0);
		
		GUITmp.normal.background = People2Texture;
		GUITmp.active.background = People2Texture;
		if (GUI.Button(Rect(Screen.width * (800.0/1444.0) , Screen.height * (300.0/963.0) ,  People2Texture.width*(240.0/212.0)*(Screen.width/1440.0)  , People2Texture.height*(670.0/351.0)*(Screen.height/963.0) ),"",GUITmp)){
			Application.LoadLevel("Game");	//改這進入遊戲 將Menu改成要進入的scene
		}
		GUI.DrawTexture(Rect(Screen.width * (10.0/1444.0) , Screen.height * (400.0/963.0) ,  People2Power.width*(742.0/843.0)*(Screen.width/1440.0)  , People2Power.height*(800.0/662.0)*(Screen.height/963.0) ), People2Power, ScaleMode.ScaleToFit, true, 0);
	}
	
	if( actor == 2){
		//GUI.DrawTexture(Rect(Screen.width * 0.5 , Screen.height * 0.22 ,  People1Texture.width*0.2  , People1Texture.height*0.2 ), People1Texture, ScaleMode.ScaleToFit, true, 0);
		//GUI.DrawTexture(Rect(Screen.width * 0.72 , Screen.height * 0.22 ,  PeopleTexture.width*0.125  , PeopleTexture.height*0.125 ), PeopleTexture, ScaleMode.ScaleToFit, true, 0);
		
		GUITmp.normal.background = People3Texture;
		GUITmp.active.background = People3Texture;
		if (GUI.Button(Rect(Screen.width * (920.0/1444.0) , Screen.height * (250.0/963.0) ,  People3Texture.width*(300.0/212.0)*(Screen.width/1440.0)  , People3Texture.height*(750.0/351.0)*(Screen.height/963.0) ),"",GUITmp)){
			Application.LoadLevel("Game");	//改這進入遊戲 將Menu改成要進入的scene
		}
		GUI.DrawTexture(Rect(Screen.width * (10.0/1444.0) , Screen.height * (400.0/963.0) ,  People3Power.width*(742.0/843.0)*(Screen.width/1440.0)  , People3Power.height*(800.0/662.0)*(Screen.height/963.0) ), People3Power, ScaleMode.ScaleToFit, true, 0);
	}	
}