var BGTexture : Texture2D;
var PlayTexture : Texture2D;
var PlayMOTexture : Texture2D;
var ExitTexture : Texture2D;
var ExitMOTexture : Texture2D;
function Start () {
    Screen.orientation = ScreenOrientation.LandscapeLeft;
}

function OnGUI(){
	
	var GUITmp : GUIStyle = GUIStyle();
	
  	GUI.DrawTexture(Rect(0 ,0 ,Screen.width ,Screen.height), BGTexture, ScaleMode.StretchToFill, true, 0);
	
	GUITmp.normal.background = PlayTexture;
	GUITmp.active.background = PlayMOTexture;
	if (GUI.Button(Rect(Screen.width * (876.0/1444.0) , Screen.height * (300.0/963.0) ,  PlayTexture.width*(420.0/460.0)*(Screen.width/1440.0)  , PlayTexture.height*(147/182.0)*(Screen.height/963.0)  ),"",GUITmp)){
		Application.LoadLevel("Setting");
	}
	GUITmp.normal.background = ExitTexture;
	GUITmp.active.background = ExitMOTexture;
	if (GUI.Button(Rect(Screen.width * (876.0/1444.0) , Screen.height * (600.0/963.0) ,  ExitTexture.width*(420.0/460.0)*(Screen.width/1440.0)  , ExitTexture.height*(147/182.0)*(Screen.height/963.0)  ),"",GUITmp)){
		Application.Quit();
	}
	
}