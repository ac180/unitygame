var BulletPrefab:Rigidbody;
var enemyai: AI;
public var speedTime:int;
private var bullet : Rigidbody ; 
private var lastFireTime:int;
public var attackDistance : int;
public var state:int;
function Start()
{
	enemyai = GetComponent(AI);
	state=0;
}
function Update () {
	if(Time.timeScale)
	{
		if(enemyai.far<attackDistance)
		{
			state=1;
			if (Time.time > lastFireTime + speedTime /10) 
			{//子彈發射速率
		 		  	fireBullet();//子彈發射函式 
		 		  	
		 		  	lastFireTime = Time.time;
			}
		}
		else
			state=0;
	}
}

function fireBullet(){
    bullet  = Instantiate (BulletPrefab, transform.position , Quaternion.identity); 
	bullet.name = "enemybullet";
	bullet.velocity = transform.TransformDirection(Vector3.forward* 10);//子彈發射方向的速度 
	Physics.IgnoreCollision(transform.collider, bullet.transform.collider);
}