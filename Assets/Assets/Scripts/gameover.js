var BGTexture : Texture2D;
private var start = 0;
private var score = score_printer.score;

var mystyle : GUIStyle;

function OnGUI(){
	
	var GUITmp : GUIStyle = GUIStyle();
	start = Time.timeSinceLevelLoad;
	
	GUITmp.normal.background = BGTexture;
	if (GUI.Button(Rect(0,0 ,  Screen.width  , Screen.height ),"",GUITmp)){
		Application.LoadLevel("Menu");
	}
	if( start >= 5.0){
		Application.LoadLevel("Menu");
	}
	GUI.Label(Rect(Screen.width*0.45,Screen.height*0.6, 100 ,100), "Score: " +score.ToString() , mystyle);
	
	if( score >= PlayerPrefs.GetInt("Player Score") ){
		PlayerPrefs.SetInt("Player Score", score);
	}
	GUI.Label(Rect(Screen.width*0.45,Screen.height*0.65,100,100), "High Score: " +PlayerPrefs.GetInt("Player Score").ToString() , mystyle);
}