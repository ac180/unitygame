var timeToLive = 6;
var bodyEmitter : ParticleEmitter;
function Start () 
{
	Destroy(gameObject, timeToLive);
	bodyEmitter = gameObject.GetComponent("Body");
	yield WaitForSeconds(bodyEmitter.minEnergy / 2);
	bodyEmitter.emit = false;
	
}