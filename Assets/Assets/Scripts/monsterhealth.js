public var health : int;
private var spawnpoint : GameObject;
private var state : int=0;
public var deathExplosion : GameObject;
public var sendDeathMessage : GameObject;

function Start()
{
	spawnpoint = GameObject.Find("enemystartpoint");

}

function OnControllerColliderHit (hit : ControllerColliderHit) {
    var body : Rigidbody = hit.collider.attachedRigidbody;
    // no rigidbody
    if (body == null || body.isKinematic)
        return;
        
    // We dont want to push objects below us
    if (body.name=="bullet") 
        health--;
    
}

function Update () {
	if(health<=0 && state==0)
	{
		state=1;
		GameObject.Destroy(this.gameObject);
		Instantiate(deathExplosion, transform.position, Quaternion.identity);
		var newEnemy:newenemy = spawnpoint.GetComponent(newenemy);
		
		newEnemy.total--;		
		
		sendDeathMessage = GameObject.Find("display_score");
		
		var sp : score_printer = sendDeathMessage.GetComponent("score_printer");
		sp.score++;
		
		state=0;
	}
}		
